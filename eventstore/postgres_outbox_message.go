package eventstore

import (
	"database/sql"
	"encoding/json"

	"github.com/jackc/pgtype"
	outbox "gitlab.com/go-lang-tools/transaction-outbox"
)

type PostgresMessage struct {
	ID         string
	EntityId   string
	EntityName string
	Published  bool
	Payload    pgtype.JSONB
	Headers    pgtype.JSONB
	Exchange   string
	RoutingKey string
	CreatedAt  pgtype.Timestamp
	UpdatedAt  sql.NullTime
}

func NewPostgresMessage(entity *outbox.Message) (*PostgresMessage, error) {
	var payloadPg pgtype.JSONB
	if err := payloadPg.Set(entity.Payload); err != nil {
		return nil, err
	}

	var headersPg pgtype.JSONB
	if err := headersPg.Set(entity.Headers); err != nil {
		return nil, err
	}

	createdAt := pgtype.Timestamp{}
	if err := createdAt.Set(entity.CreatedAt); err != nil {
		return nil, err
	}

	return &PostgresMessage{
		ID:         entity.ID,
		Payload:    payloadPg,
		Headers:    headersPg,
		Published:  entity.Published,
		EntityId:   entity.EntityId,
		EntityName: entity.EntityName,
		Exchange:   entity.Exchange,
		RoutingKey: entity.RoutingKey,
		CreatedAt:  createdAt,
		UpdatedAt:  sql.NullTime{Time: entity.UpdatedAt, Valid: !entity.UpdatedAt.IsZero()},
	}, nil
}

func (e *PostgresMessage) GetEntity() (*outbox.Message, error) {
	var headers map[string]interface{}
	if err := json.Unmarshal(e.Headers.Bytes, &headers); err != nil {
		return nil, err
	}

	return &outbox.Message{
		ID:         e.ID,
		Payload:    e.Payload,
		Headers:    headers,
		Published:  e.Published,
		EntityId:   e.EntityId,
		EntityName: e.EntityName,
		Exchange:   e.Exchange,
		RoutingKey: e.RoutingKey,
		CreatedAt:  e.CreatedAt.Time,
		UpdatedAt:  e.UpdatedAt.Time,
	}, nil
}
