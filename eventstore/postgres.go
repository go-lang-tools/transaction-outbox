package eventstore

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/go-lang-tools/tools/repositories"
	outbox "gitlab.com/go-lang-tools/transaction-outbox"
)

// Postgres is the implementation of the event store
type Postgres struct {
	*repositories.Repositories
	table string
}

// NewPostgres constructs Postgres with all dependencies.
func NewPostgres(rep *repositories.Repositories, table string) (*Postgres, error) {
	if rep == nil {
		return nil, fmt.Errorf("%s: %w", "NewPostgres", errors.New("nil repositories"))
	}

	if table == "" {
		return nil, fmt.Errorf("%s: %w", "NewPostgres", errors.New("need table name"))
	}

	return &Postgres{
		Repositories: rep,
		table:        table,
	}, nil
}

// AddWithinTx creates a transaction and then tries to execute anything within it
func (p *Postgres) AddWithinTx(ctx context.Context, messages []*outbox.Message, fn func(ctx context.Context) error) error {
	tx, err := p.StartTransaction(ctx)
	if err != nil {
		return fmt.Errorf("transaction start failed: %w", err)
	}

	err = fn(tx)
	if err != nil {
		return err
	}

	// nolint
	query := fmt.Sprintf(
		`
INSERT INTO %s(
		id, 
		entity_id, 
		entity_name, 
		payload, 
		headers, 
		published, 
		created_at, 
		updated_at, 
        exchange, 
        routing_key
	) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
`,
		p.table,
	)

	db := p.GetConnect(tx)
	var pgMessage *PostgresMessage
	for _, message := range messages {
		pgMessage, err = NewPostgresMessage(message)
		if err != nil {
			if err = p.CancelTransaction(tx); err != nil {
				return err
			}

			return fmt.Errorf("failed to create posgres message: %w", err)
		}

		_, err = db.Exec(
			tx,
			query,
			pgMessage.ID,
			pgMessage.EntityId,
			pgMessage.EntityName,
			pgMessage.Payload,
			pgMessage.Headers,
			pgMessage.Published,
			pgMessage.CreatedAt,
			pgMessage.UpdatedAt,
			pgMessage.Exchange,
			pgMessage.RoutingKey,
		)
		if err != nil {
			if err = p.CancelTransaction(tx); err != nil {
				return err
			}

			return fmt.Errorf("failed to insert message into the event store: %w", err)
		}
	}

	if err = p.StopTransaction(tx); err != nil {
		return fmt.Errorf("transaction commit failed: %w", err)
	}

	return nil
}

// SetAsDispatched sets one message as dispatched
func (p *Postgres) SetAsDispatched(ctx context.Context, id string) error {
	query := fmt.Sprintf(`
UPDATE %s
SET
    published = true,
    updated_at = $1
WHERE id = $2;
`, p.table)
	if _, err := p.DB.Exec(ctx, query, time.Now(), id); err != nil {
		return fmt.Errorf("failed to set message as dispatched: %w", err)
	}

	return nil
}

// Remove removes old messages from the data store
func (p *Postgres) Remove(ctx context.Context, dispatchedBefore time.Time, batchSize int32) error {
	_, err := p.StartTransaction(ctx)
	if err != nil {
		return fmt.Errorf("transaction start failed: %w", err)
	}

	q := `
DELETE FROM %[1]s
WHERE ctid IN
(
    select ctid
    from %[1]s
    where
        "published" = true and
        "updated_at" < $1
    limit %d
)
`
	db := p.GetConnect(ctx)
	query := fmt.Sprintf(q, p.table, batchSize)
	if _, err = db.Exec(ctx, query, dispatchedBefore); err != nil {
		if err = p.CancelTransaction(ctx); err != nil {
			return err
		}

		return fmt.Errorf("failed to remove messages from the data store: %w", err)
	}

	if err = p.StopTransaction(ctx); err != nil {
		return fmt.Errorf("transaction commit failed: %w", err)
	}

	return nil
}
