# Transaction outbox

Реализация на Go Lang шаблона: [Transactional outbox](https://microservices.io/patterns/data/transactional-outbox.html)

## Использование

Установка:

```
go get gitlab.com/go-lang-tools/transaction-outbox
```

## Настройка
Ниже пример базовой настройки с использованием сервера базы данных `Postgres` и брокера сообщений `RabbitMQ`

```go
package main

import (
	"context"
	"fmt"

	"gitlab.com/go-lang-tools/events/rabbitmq"
	"gitlab.com/go-lang-tools/tools/repositories"

	outbox "gitlab.com/go-lang-tools/transaction-outbox"
	"gitlab.com/go-lang-tools/transaction-outbox/eventstore"
	"gitlab.com/go-lang-tools/transaction-outbox/stream"
)

func main() {
    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()
    
    var rep *repositories.Repositories
    // ..instance rep
    ds, err := eventstore.NewPostgres(rep, "table_name")
    
    var client *rabbitmq.RabbitMQ
    // ..instance client
    es, err := stream.NewAMQP(client, stream.Options{})
    
    o, err := outbox.NewOutBoxer(
		outbox.WithDataStore(ds),
		outbox.WithEventStream(es),
    )
    if err != nil {
        fmt.Printf("could not create an outboxer instance: %s", err)
        return
    }
    
	var messages []*outbox.Message
	err = o.SendWithinTx(ctx, messages, func(ctx context.Context) error {
		// save data into DB
		return nil
    })
}
```

Пример схемы таблицы
```postgresql
create table if not exists public.events
(
    id          uuid                                        not null
        constraint events_pkey
            primary key,
    entity_id   uuid                                        not null,
    entity_name varchar(50)                                 not null,
    payload     jsonb                                       not null,
    headers     jsonb                                       not null,
    published   bool                                        not null,
    created_at  timestamp                                   not null,
    updated_at  timestamp,
    exchange    varchar(25)                                 not null,
    routing_key varchar(50)                                 not null
);

create index if not exists events_created_at_published_indx
    on events (created_at, published)
    where published = false;
```

## TODO
- Сделать переотправку не опубликованных сообщений из БД
- Написать тесты
- Удаление отправленных событий. Вкл по опции