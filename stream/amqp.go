// Package stream is the AMQP implementation of an event stream.
package stream

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.com/go-lang-tools/events/rabbitmq"
	outbox "gitlab.com/go-lang-tools/transaction-outbox"
)

const defaultExchangeType = "topic"

// AMQP is the wrapper for the AMQP library
type AMQP struct {
	client  *rabbitmq.RabbitMQ
	options Options
}

type Options struct {
	Exchange     string
	ExchangeType string
	Durable      bool
	AutoDelete   bool
	Internal     bool
	NoWait       bool
}

// NewAMQP creates a new instance of AMQP
func NewAMQP(client *rabbitmq.RabbitMQ, options Options) (*AMQP, error) {
	if client == nil {
		return nil, fmt.Errorf("%s: %w ", "NewAMQP", errors.New("nil client"))
	}

	return &AMQP{
		client:  client,
		options: options,
	}, nil
}

func (a *AMQP) InitExchange() error {
	if a.options.Exchange == "" {
		return fmt.Errorf("%s: %w ", "InitExchange", errors.New("empty Exchange name"))
	}

	if a.options.ExchangeType == "" {
		a.options.ExchangeType = defaultExchangeType
	}

	ch := a.client.GetChannel()
	if ch == nil {
		return errors.New("channel is closed ")
	}

	if err := ch.ExchangeDeclare(
		a.options.Exchange,
		a.options.ExchangeType,
		a.options.Durable,
		a.options.AutoDelete,
		a.options.Internal,
		a.options.NoWait,
		nil,
	); err != nil {
		return fmt.Errorf("Exchange declare: %s", err)
	}

	return nil
}

// Send sends the message to the event stream
func (a *AMQP) Send(ctx context.Context, message *outbox.Message) error {
	ch := a.client.GetChannel()
	if ch == nil {
		return errors.New("channel is closed ")
	}

	body, err := json.Marshal(message.Payload)
	if err != nil {
		return fmt.Errorf("massage payoad marshaling failed: %w", err)
	}

	if err = ch.Publish(
		message.Exchange,
		message.RoutingKey,
		false,
		false,
		amqp.Publishing{
			ContentType:  "application/json",
			Body:         body,
			DeliveryMode: amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:     0,              // 0-9
			Headers:      message.Headers,
		},
	); err != nil {
		return fmt.Errorf("Exchange publish: %s", err)
	}

	return nil
}
