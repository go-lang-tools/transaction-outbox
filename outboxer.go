package outbox

import (
	"context"
	"errors"
	"time"

	"github.com/rs/zerolog/log"
)

var ErrMissingEventStore = errors.New("a event store is required for the outboxer to work")

// EventStore defines the data store methods
type EventStore interface {
	AddWithinTx(ctx context.Context, messages []*Message, fn func(ctx context.Context) error) error
	SetAsDispatched(ctx context.Context, id string) error
	Remove(ctx context.Context, since time.Time, batchSize int32) error
}

type EventStream interface {
	Send(context.Context, *Message) error
}

// OutBoxer implements the outbox pattern
type OutBoxer struct {
	ds EventStore
	es EventStream
}

func NewOutBoxer(opts ...Option) (*OutBoxer, error) {
	o := OutBoxer{}
	for _, opt := range opts {
		opt(&o)
	}

	if o.ds == nil {
		return nil, ErrMissingEventStore
	}

	return &o, nil
}

// SendWithinTx encapsulate any database call within a transaction
func (o *OutBoxer) SendWithinTx(ctx context.Context, messages []*Message, fn func(ctx context.Context) error) error {
	if err := o.ds.AddWithinTx(ctx, messages, fn); err != nil {
		return err
	}

	go o.send(messages)
	return nil
}

func (o *OutBoxer) send(messages []*Message) {
	ctx := context.Background()

	for _, message := range messages {
		if err := o.es.Send(ctx, message); err != nil {
			log.Warn().Err(err).Msg("can not send event to stream")

			continue
		}

		if err := o.ds.SetAsDispatched(ctx, message.ID); err != nil {
			log.Warn().Err(err).Msgf("can not set as dispatched message %s ", message.ID)
		}
	}
}
