package outbox

import (
	"time"

	"github.com/google/uuid"
)

type Message struct {
	ID         string
	EntityId   string
	EntityName string
	Published  bool
	Payload    interface{}
	Headers    map[string]interface{}
	Exchange   string
	RoutingKey string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

func NewMessage(payload interface{}, headers map[string]interface{}, entityId, entityName, routingKey, exchange string) *Message {
	return &Message{
		ID:         uuid.NewString(),
		Payload:    payload,
		Headers:    headers,
		Published:  false,
		EntityId:   entityId,
		EntityName: entityName,
		RoutingKey: routingKey,
		Exchange:   exchange,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Time{},
	}
}
